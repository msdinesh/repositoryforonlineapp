<%@ Page language="c#" MasterPageFile="~/ApplicationFormGeneral.Master" Codebehind="CreateAccount.aspx.cs" AutoEventWireup="True" Inherits="ApplicationForm.CreateAccount" %>
<%@ MasterType TypeName = "ApplicationForm.ApplicationFormGeneral" %>
<asp:Content ID="Content1" ContentPlaceHolderID="uxContentPlaceHolderMain" runat="server">
		
        An asterisk(*) indicates a required field.</p>
    <h2>
        Create an account with VSO UK</h2>
    <p>
        To access the application form, we need you to create a VSO account. Please choose
        a username and password .</p>

<p>
To access the application first fill the registration form 
</p>    <br />
    <br />
    <label for="uxTestEmail" class="column">
        Email address</label>&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="uxTestEmail" runat="server"></asp:Label>
    <asp:Label Visible="false" ID="uxTestUserID" runat="server"></asp:Label>
    <br />
    <asp:Panel ID="panDisplayUserEntry" runat="server">
    </asp:Panel>
    <br />
    <span class="mandatory">*</span>
    <label for="txtFirstname" class="column">
        Firstname</label>
    <asp:TextBox ID="txtFirstname" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
    <br />
    <br />
    <span class="mandatory">*</span>
    <label for="txtSurname" class="column">
        Surname</label>
    <asp:TextBox ID="txtSurname" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
    <br />
    <br />
    <!-- Added by dev2 -->
    <span class="mandatory">*</span>
    <label for="txtGender" class="column">
        Gender</label>
    <asp:TextBox ID="txtGender" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
    <br />
    <br />
    <span class="mandatory">*</span>
    <label for="txtUsername" class="column">
        Chosen Username</label>
    <asp:TextBox ID="txtUsername" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
    <br />
    <br />
    <span class="mandatory">*</span>
    <label for="txtPassword1" class="column">
        Chosen Password</label>
    <asp:TextBox ID="txtPassword1" runat="server" MaxLength="50" TextMode="Password"
        Width="250px"></asp:TextBox>
    <br />
    <label for="lblMaxChar" class="column">
    </label>
    <asp:Label ID="lblMaxChar" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;(Minimum 7 characters)</asp:Label>
    <br />
    <span class="mandatory">*</span>
    <label for="txtPassword2" class="column">
        Confirm Password</label>
    <asp:TextBox ID="txtPassword2" runat="server" MaxLength="50" TextMode="Password"
        Width="250px"></asp:TextBox>
    <br />
    <br />
    <span class="mandatory">*</span>
    <label for="txtQuestion" class="column">
        Security Question</label>
    <asp:TextBox ID="txtQuestion" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
    <br />
    <label for="lblSecEx" class="column">
    </label>
    <asp:Label runat="server" ID="lblSecEx">&nbsp;&nbsp;&nbsp;&nbsp;e.g Mother's maiden name.</asp:Label>
    <br />
    <span class="mandatory">*</span>
    <label for="txtAnswer" class="column">
        Security Answer</label>
    <asp:TextBox ID="txtAnswer" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
    <br />
    <br />
    <asp:Label ID="lblErrorMessage" CssClass="errormessage" runat="server"></asp:Label>
    <br />
    <label for="btnCreateAccount" class="column">
        &nbsp;</label>
    <asp:Button ID="btnCreateAccount" runat="server" Text="Create Account" OnClick="btnCreateAccount_Click" />
    <br />
    <br />
    <asp:Label ID="lblResult" runat="server" EnableViewState="False" Visible="false">Your account has been created successfully, <a href="Default.aspx">please now login.</a></asp:Label>
    <br />
</asp:Content>

